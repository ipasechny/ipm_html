var gulp = require('gulp'),

	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	postcss = require('gulp-postcss'),
	imagemin = require('gulp-imagemin'),
	favicons = require('gulp-favicons'),
	repath = require('gulp-replace-path'),
	sourcemaps = require('gulp-sourcemaps'),
	browserSync = require('browser-sync').create(),
	reload = browserSync.reload,
	src = {
		js: 'assets/js/',
		css: 'assets/css/',
		img: 'assets/img/',
		data: 'assets/data/',
		icons: 'assets/icons/',
		fonts: 'assets/fonts/'
	},
	
    args = require('yargs').argv,
    env  = args.env || 'ipm';

//--env=ipm|kef|beroc|research
var themeName = /^(ipm|kef|beroc|research)$/.test(env) ? env : 'ipm';

function em(px, base) {
	base = base ? base : 10;
	return round((px / base) + 0.0001, 4);
}
function round(value, decimals) {
	return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

//#region [js][:]
gulp.task('js', function(done) {
	gulp.src([
			'source/js/jquery.min.js',
			'source/js/plugin.min.js'
		])
		.pipe(uglify())
		.pipe(concat('bundle.min.js'))
		.pipe(gulp.dest(src.js))
		.pipe(browserSync.stream());

	gulp.src('source/js/script.js')
		.pipe(gulp.dest(src.js))
		.pipe(browserSync.stream());

	done();
});
//#endregion [js][;]

//#region [css][:]
gulp.task('css', function(done) {
	gulp.src('source/css/index-'+ themeName +'.css')
		.pipe(sourcemaps.init())
		.pipe(rename('style.min.css'))
		.pipe(postcss())
		.pipe(repath(/\/assets\//g, '../'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(src.css))
		.pipe(browserSync.stream());

	done();
});
//#endregion [css][;]

//#region [img][:]
gulp.task('img', function(done) {
	gulp.src('source/img/**/*')
		.pipe(
			imagemin([
				imagemin.svgo(),
				imagemin.optipng(),
				imagemin.gifsicle(),
				imagemin.jpegtran()
			])
		)
		.pipe(gulp.dest(src.img))
		//.pipe(browserSync.stream());

	done();
});
//#endregion [img][;]

//#region data][:]
gulp.task('data', function(done) {
	gulp.src('source/data/**/*.*')
		.pipe(gulp.dest(src.data))
		.pipe(browserSync.stream());

	done();
});
//#endregion [data][;]

//#region [fonts][:]
gulp.task('fonts', function(done) {
	gulp.src(['source/fonts/*.*'])
		.pipe(gulp.dest('assets/fonts/'))
		.pipe(browserSync.stream());

	done();
});
//#endregion [fonts][;]

//#region [media][:]
gulp.task('media', function(done) {
	gulp.src(['source/media/**/*.*'])
		.pipe(gulp.dest('assets/media/'))
		.pipe(browserSync.stream());

	done();
});
//#endregion [media][;]

//#region [favicon][:]
gulp.task('favicon', function(done) {
	var settings = {
		background: 'transparent',
		icons: {
			coast: false,
			yandex: false,
			firefox: false,
			windows: false,
			android: false,
			favicons: true,
			appleIcon: true,
			appleStartup: false,
		}
	};

	gulp.src('source/img/favicon_'+ themeName +'.{svg,png}')
		.pipe(favicons(settings))
		.pipe(gulp.dest('assets/icons/'+ themeName +'/'));

	done();
});
//#endregion [favicon][;]

//#region [server][:]
gulp.task('watcher', function(done) {
	gulp.watch('source/js/**/*.*', gulp.series('js'));
	gulp.watch('source/css/**/*.*', gulp.series('css'));
	gulp.watch('source/img/**/*.*', gulp.series('img'));
	gulp.watch('source/data/**/*.*', gulp.series('data'));
	gulp.watch('source/fonts/*.*', gulp.series('fonts'));
	gulp.watch('source/media/**/*.*', gulp.series('media'));

	done();
});


gulp.task('server', function(done) {
	browserSync.init({
		notify: false,
		server: {
			baseDir: './'
		}
	});

	gulp.watch('*.html').on('change', function() {
		reload();
		done();
	});
	gulp.watch('assets/**/*').on('change', function() {
		reload();
		done();
	});

	done();
});
//#endregion [server][;]

gulp.task('default',
	gulp.series(
		'img',
		'js',
		'css',
		'data',
		'fonts',
		'media',
		'server',
		'watcher'
	)
);