(function(global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
		typeof define === 'function' && define.amd ? define(factory) :
		(global.shave = factory());
}(this, (function() {
	'use strict';

	function shave(target, maxHeight) {
		var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

		if (!maxHeight) throw Error('maxHeight is required');
		var els = typeof target === 'string' ? document.querySelectorAll(target) : target;
		if (!els) return;

		var btnText = opts.btnText || 'More';
		var character = opts.character || '…';
		var classname = opts.classname || 'js_shave_full';
		var spaces = opts.spaces || true;
		var charHtml = '<span class="js_shave_char">' + character + '</span>';

		if (!('length' in els)) els = [els];
		for (var i = 0; i < els.length; i += 1) {
			var el = els[i];
			var styles = el.style;
			var span = el.querySelector('.' + classname);
			var textProp = el.textContent === undefined ? 'innerText' : 'textContent';
			
			var shortText = '';
			var htmlContent = el.innerHTML;

			// If element text has already been shaved
			if (span) {
				// Remove the ellipsis to recapture the original text
				el.removeChild(el.querySelector('.js-shave-char'));
				el[textProp] = el[textProp]; // nuke span, recombine text
			}

			var fullText = el[textProp];
			var words = spaces ? fullText : fullText.split(' ');

			// If 0 or 1 words, we're done
			if (words.length < 2) continue;

			// Temporarily remove any CSS height for text height calculation
			var heightStyle = styles.height;
			styles.height = 'auto';
			var maxHeightStyle = styles.maxHeight;
			styles.maxHeight = 'none';

			// If already short enough, we're done
			if (el.offsetHeight <= maxHeight) {
				styles.height = heightStyle;
				styles.maxHeight = maxHeightStyle;
				continue;
			}
			
			el.className += ' _shave';

			// Binary search for number of words which can fit in allotted height
			var max = words.length - 1;
			var min = 0;
			var pivot = void 0;
			while (min < max) {
				pivot = min + max + 1 >> 1; // eslint-disable-line no-bitwise
				el[textProp] = spaces ? words.slice(0, pivot) : words.slice(0, pivot).join(' ');
				el.insertAdjacentHTML('beforeend', charHtml);
				if (el.offsetHeight > maxHeight) max = spaces ? pivot - 2 : pivot - 1;
				else min = pivot;
			}

			//el[textProp] = spaces ? words.slice(0, max) : words.slice(0, max).join(' ');
			//el.insertAdjacentHTML('beforeend', charHtml);

			//var diff = spaces ? words.slice(max) : words.slice(max).join(' ');
			//el.insertAdjacentHTML('beforeend', '<span class="' + classname + '" style="display:none;">' + diff + '</span>');

			shortText = spaces ? words.slice(0, max) : words.slice(0, max).join(' ');

			el.innerHTML = 
				'<span class="shave_trim js_shave_trim">'+ shortText + charHtml +'</span>'+
				'<span class="shave_more js_shave_more">'+ btnText +'</span>';

			el.insertAdjacentHTML('beforeend', '<div class="shave_full' + classname + '" style="display:none;">' + htmlContent + '</div>');

			styles.height = heightStyle;
			styles.maxHeight = maxHeightStyle;
		}
	}

	return shave;

})));