// #region [maps]
function initMap() {
  if ($('#map').length) {
    var map;
    var marker;

    var isHashChangeOFF = false;

    var pageHash = location.hash;

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: places[0],
    });

    for (var i = 0; i < places.length; i++) {
      marker = new google.maps.Marker({
        map: map,
        position: places[i],
      });
    }

    var changeCity = function ($el, hash) {
      var isLink = $el.length;

      if (!isLink && !hash) {
        $link = $('.mcity_link:eq(0)');
      } else {
        $link = isLink
          ? $el
          : $('.mcity_link[href="' + hash + '"]');
      }

      if ($link.length) {
        var index = $link.index();
        var $tab = $('.office_item').eq(index);

        map.panTo(places[index]);

        $link
          .add($tab)
          .addClass('_active')
          .siblings()
          .removeClass('_active');
      }
    };

    $('.mcity_link').on('click', function () {
      isHashChangeOFF = true;

      changeCity($(this));
    });

    if (pageHash) {
      changeCity(false, pageHash);
    }

    $(window).on('hashchange', function () {
      if (!isHashChangeOFF) {
        changeCity(false, location.hash);
      }

      isHashChangeOFF = false;
    });
  }
}
// #endregion [maps]

// #region [youtube]
if ($('.js_open_video').length) {
  var tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/iframe_api';
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var ytPlayer = !1;

  var ytPlayerStop = function () {
    ytPlayer.stopVideo();
  };

  var ytPlayerVideo = function (src) {
    if (ytPlayer) {
      var thisURL = ytPlayer.getVideoUrl();
      var thisID = thisURL.split('=')[1];

      if (src !== thisID) {
        ytPlayer.cueVideoById(src);
      }

      ytPlayer.playVideo();
    } else {
      ytPlayer = new YT.Player('youtube', {
        videoId: src,
        width: '640',
        height: '360',
        playerVars: {
          rel: 0,
          showinfo: 0,
          autoplay: 1,
        },
      });
    }
  };

  // function onYouTubeIframeAPIReady() {}
}
// #endregion [youtube]

$(function () {

  var $gWin = $(window);
  // var scrlSize = scrollbarSize();
  var mobile = 'screen and (max-width: 768px)';
  var tablet = 'screen and (min-width: 769px)';

  // #region    [!]----------[ FUNC ]----------[!]
  function fnHideScroll() {
    $('html, body').css({
      overflow: 'hidden',
      // paddingRight: scrlSize,
    });
  }
  function fnShowScroll() {
    $('html, body').css({
      overflow: '',
      // paddingRight: '',
    });
  }
  function runIfFunc(func) {
    if (typeof func === 'function') {
      func();
    }
  }
  // #endregion [!]----------[ FUNC ]----------[!]

  // #region    [!]----------[ BASE ]----------[!]

  // #region [up]
  (function () {
    var scrollOn = !1;
    var $btn = $('.js_up');

    $gWin.on('scroll.up', function () {
      if (!scrollOn) {
        scrollOn = true;

        requestAnimationFrame(function () {
          var win = window;
          var winH = win.innerHeight;
          var docH = document.body.scrollHeight;
          var winTop = win.pageYOffset;
          var winBot = winTop + winH;

          if (winBot > docH - 29) {
            $btn.addClass('_up');
          } else {
            $btn.removeClass('_up');
          }

          if (winTop > winH / 2) {
            $btn.addClass('_show');
          } else {
            $btn.removeClass('_show');
          }

          scrollOn = false;
        });
      }
    }).trigger('scroll.up');

    $btn.on('click', function () {
      $('html, body').animate({ scrollTop: 0 }, 400);
      return false;
    });
  }());
  // #endregion [up]

  // #region [plan]
  var getPlanTimeout;
  var $mPlan = $('.js_mplan');
  var $mPlanView = $('.plan_view');
  var $mPlanAll = $('.js_mplan_all');
  var $fPlanReset = $('.fplan_reset');

  $('#fplan')
    .on('submit', function () {
      getPlan();

      $fPlanReset.prop('disabled', false);
      $mPlanAll.prop('selected', true);
      $mPlan.selectOrDie('update');
      return false;
    });
  $fPlanReset.on('click', function () {
    var $form = $('#fplan');

    $form[0].reset();
    $form.find('select').selectOrDie('update');
    $fPlanReset.prop('disabled', true);

    getPlan();
  });
  $('.js_mplan').on('change', function () {
    $fPlanReset.prop('disabled', false);

    getPlan();
  });
  $(document).on('click', '.js_plan_nav', function () {
    var boxY = $('.plan_view').offset().top;

    $('html, body').animate({
      scrollTop: boxY - 10,
    }, 400);

    getPlan($(this).attr('href'));

    return false;
  });

  function setPlan(data) {
    var nav = data.nav;
    var output = data.html;
    var btnPrev = '';
    var btnNext = '';

    if (nav) {
      var lnkPrev = nav.prev;
      var lnkNext = nav.next;

      if (lnkPrev || lnkNext) {
        if (lnkPrev) {
          btnPrev = '<a class="plan_arrow _prev js_plan_nav" href="' + lnkPrev + '"></a>';
        } else {
          btnPrev = '<span class="plan_arrow _prev _disable"></span>';
        }

        if (lnkNext) {
          btnNext = '<a class="plan_arrow _next js_plan_nav" href="' + lnkNext + '"></a>';
        } else {
          btnNext = '<span class="plan_arrow _next _disable"></span>';
        }
      }
    }

    output += (btnPrev + btnNext);
    $('.plan_view').html(output);
  }

  function getPlan(url) {
    clearTimeout(getPlanTimeout);
    getPlanTimeout = setTimeout(function () {
      var $form = $('#fplan');
      var data = $form.serialize() + '&' + $('.js_mplan').attr('name') + '=' + $('.js_mplan').val();

      url = url || $form.attr('action');
      $mPlanView.addClass('_loading');
      // preloaderShow();

      $.ajax({
        url: url,
        data: data,
        type: 'get',
        dataType: 'json',
        success: function (response) {
          setPlan(response);
        },
        complete: function () {
          // preloaderHide();
          $mPlanView.removeClass('_loading');
        },
      });
    }, 200);
  }
  // #endregion [plan]

  // #region [news]
  $('.js_news_tabs').on('change', function () {
    var index = $(this).val();
    var $item = $('.news_tab').eq(index);
    $item.addClass('_active').siblings().removeClass('_active');
  });

  $('.js_news_filter').on('change', function () {
    var $this = $(this);
    var thisValue = $this.val();
    var prevValue = $this.data('prev-value');
    var $item = $this.find('[value="' + thisValue + '"]');

    if (
      (prevValue !== undefined && thisValue !== prevValue)
      || (prevValue === undefined && $item.index() > 0)
    ) {
      var url = $this.data('url');
      var $body = $this.closest('.news').find('.news_body');

      $this.data('prev-value', thisValue);

      $.ajax({
        url: url,
        type: 'post',
        data: {
          type: thisValue,
        },
        success: function (data) {
          $body.html(data);
        },
      });

      /*
      if (type !== 'all') {
        $('.news_item._mix').addClass('_hide');
        $('.news_item._mix_' + type).removeClass('_hide');
      } else {
        $('.news_item').removeClass('_hide');
      }
      */
    }
  });

  $('.js_tnews_menu').on('change', function () {
    var $this = $(this);
    var index = parseInt($this.val());
    var $item = $('.tnews_item').eq(index);

    $item.addClass('_active').siblings().removeClass('_active');
  });
  // #endregion [news]

  // #region [share]
  var Share = {
    vk: function () {
      var o = this;
      var url = 'https://vk.com/share.php?url=';

      url += o.url();
      o.popup(url);
    },
    ok: function () {
      var o = this;
      var url = 'https://connect.ok.ru/dk?cmd=WidgetSharePreview&st.cmd=WidgetSharePreview&st._aid=ExternalShareWidget_SharePreview&st.shareUrl=';

      url += o.url();
      o.popup(url);
    },
    fb: function () {
      var o = this;
      var url = 'https://www.facebook.com/sharer/sharer.php?u=';

      url += o.url();
      o.popup(url);
    },
    tw: function () {
      var o = this;
      var url = 'https://twitter.com/intent/tweet?url=';

      url += o.url();
      o.popup(url);
    },
    gp: function () {
      var o = this;
      var url = 'https://plus.google.com/share?url=';

      url += o.url();
      o.popup(url);
    },
    tg: function () {
      var o = this;
      var url = 'https://t.me/share/url?url=';

      url += o.url();
      o.popup(url);
    },
    in: function () {
      var o = this;
      var url = 'https://www.linkedin.com/shareArticle?url=';

      url += o.url();
      o.popup(url);
    },
    url: function () {
      var href = location.href;
      var url = encodeURIComponent(href);
      return url;
    },
    popup: function (url) {
      window.open(url, '_blank');
    },
  };

  $('.js_share').on('click', function () {
    var $this = $(this);
    var type = $this.data('type');

    switch (type) {
      case 'vk': Share.vk(); break;
      case 'ok': Share.ok(); break;
      case 'fb': Share.fb(); break;
      case 'tw': Share.tw(); break;
      case 'gp': Share.gp(); break;
      case 'tg': Share.tg(); break;
    } return false;
  });
  // #endregion [share]

  // #region [about]
  $('.js_about_tabs').on('change', function () {
    var index = $(this).val();
    var $item = $('.about_tab').eq(index);
    $item.addClass('_active').siblings().removeClass('_active');
  });
  // #endregion [about]

  // #region [bayan]
  $('.bayan_head').on('click', function () {
    var $this = $(this);
    var $item = $this.parent();
    if (!$this.is(':last-child')) {
      if ($item.hasClass('_active')) {
        $item.removeClass('_active');
      } else {
        $item.addClass('_active').siblings().removeClass('_active');
      }
    }
  });
  // #endregion [bayan]

  // #region [review]
  var $reviewBody = $('.review_body');
  var reviewMoreLbl = $('.review').data('more');

  shave('.review_item_text', 150, {
    btnText: reviewMoreLbl,
  });

  $('.js_shave_more').on('click', function () {
    var $this = $(this);
    var $trim = $this.prev();
    var $full = $this.next();
    $this.toggleClass('_active');
    $trim.add($full).toggle();
  });

  $(document).on('click', '.review_more', function () {
    var $this = $(this);
    var url = $this.attr('href');

    $.ajax({
      url: url,
      type: 'post',
      success: function (data) {
        $this.remove();
        $reviewBody.append(itms);
        shave('.review_item_text', 150, {
          btnText: reviewReadLbl,
        });
      },
    });

    return false;
  });
  // #endregion [review]

  // #region [scroll to]
  $('.js_scroll_to').on('click', function () {
    var href = $(this).attr('href');
    var $item = $(href);

    if ($item.length) {
      var y = $item.offset().top - 20;

      $('html, body').animate({
        scrollTop: y,
      }, 400);
    } return false;
  });
  // #endregion [scroll to]

  // #region [clipboard]
  $('.js_save_link').on('click', function () {
    var $this = $(this);
    var input = document.createElement('input');
    var href = location.origin + location.pathname;

    if (!$this.hasClass('_done')) {
      document.body.appendChild(input);
      input.value = href;
      input.select();
      document.execCommand('copy');
      $(input).remove();

      $this.addClass('_done');
      setTimeout(function () {
        $this.removeClass('_done');
      }, 800);
    } return false;
  });
  // #endregion [clipboard]

  // #region [preloader]
  var $preloader = $('#preloader');

  function preloaderShow() {
    $preloader.addClass('_ready').delay(10).queue(function () {
      $preloader.addClass('_show').dequeue();
    });
  }
  function preloaderHide() {
    $preloader.removeClass('_show').delay(10).queue(function () {
      $preloader.removeClass('_ready').dequeue();
    });
  }
  // #endregion [preloader]

  // #endregion [!]----------[ BASE ]----------[!]

  // #region    [!]----------[ MENU ]----------[!]

  // #region [msub]
  var $msub = $('.msub');
  var msubTimeout;

  function msubMinHeight() {
    $msub.removeAttr('style').each(function (i, menu) {
      var $menu = $(menu);
      var $menuUl = $menu.find('.js_sub');
      var heightArray = [];

      $menuUl.each(function (i, ul) {
        heightArray.push(
          $(ul).height()
        );
      });

      $menu.css({
        minHeight: Math.max.apply(Math, heightArray),
      });
    });
  }
  // #endregion [msub]

  // #region [mmain]
  $('.mmain_back').on('click', function () {
    var $this = $(this);
    var $menuL1 = $('.js_sub1');
    var $menuL2 = $('.js_sub2');

    $('.msub_ul1, .msub_ul2').scrollTop(0);

    if ($menuL2.hasClass('_show')) {
      $menuL2.removeClass('_show');
    } else {
      $menuL1.add($this).removeClass('_show');
    }
  });

  $('.mmain_switch').on('click', function () {
    var $this = $(this);
    var $menu = $('.mmain');
    var $sub = $('.js_sub');

    if ($this.hasClass('_active')) {
      $menu.add($sub).removeClass('_show');
      $this.removeClass('_active');
      fnShowScroll();
    } else {
      $this.addClass('_active');
      $menu.addClass('_show');
      fnHideScroll();
    }
  });

  function mmainReset() {
    fnShowScroll();
    $('.mmain_switch').removeClass('_active');
    $('.mmain, .js_sub').removeClass('_show');
  }
  // #endregion [mmain]

  // #region [mpage]
  if ($('.mpage').length) {
    var mpageTimeout;
    var mpageShowInit = false;
    var $mpageShow = $('.mpage_box');

    var mpageShowOn = function () {
      if (!mpageShowInit) {
        mpageShowInit = true;

        $mpageShow.flickity({
          contain: true,
          pageDots: false,
          groupCells: true,
        });
      }
    };

    var mpageShowOff = function () {
      if ($('.mpage').hasClass('flickity-enabled')) {
        $mpageShow.flickity('destroy');
      }
    };

    $('body').scrollspy({
      offset: 200,
      target: '.mpage',
    });

    $('.mpage_link').on('click', function () {
      var y = 0;
      var $this = $(this);
      var $trgt = $($this.attr('href'));

      if ($trgt.length) {
        y = $trgt.offset().top - 80;
        $('html, body').animate({ scrollTop: y }, 400);
      } return false;
    });

    $(window)
      .on('scroll', function () {
        var yW = $(window).scrollTop();
        var yM = $('.mpage').offset().top;

        if (yW >= yM) {
          $('.mpage_box').addClass('_fix');
        } else {
          $('.mpage_box').removeClass('_fix');
        }
      })
      .on('activate.bs.scrollspy', function () {
        var $link = $('.js_spy_active');
        var index = $link.index();

        if (mpageShowInit) {
          var thisIndex = $mpageShow.selectedIndex;

          clearTimeout(mpageTimeout);
          mpageTimeout = setTimeout(function () {
            if (thisIndex !== index) {
              $mpageShow.flickity('select', index);
            }
          }, 400);
        }
      })
      .trigger('scroll');
  }
  // #endregion [mpage]

  // #region [mpart]
  $('.js_mpart option').prop('selected', function () {
    return this.defaultSelected;
  });
  // #endregion [mpart]

  // #region [mcourse]
  (function () {
    var $show = !1;
    var resizeOn = !1;
    var reloadTimeout = !1;

    $show = $('.mcourse_show').flickity({
      contain: true,
      pageDots: false,
      groupCells: true,
      cellAlign: 'left',
      percentPosition: false,
    });

    $gWin.on('scroll.up', function () {
      if (!resizeOn) {
        resizeOn = true;

        requestAnimationFrame(function () {
          clearTimeout(reloadTimeout);
          reloadTimeout = setTimeout(function () {
            if ($show) {
              $show.flickity('reloadCells');
            }

            resizeOn = false;
          }, 200);
        });
      }
    }).trigger('scroll.up');

    $('.js_mcourse').on('change', function () {
      filterSet($(this).val());
    });
    $('.mcourse_link').on('click', function () {
      var $this = $(this);
      var type = $this.data('type');

      $this.addClass('_active').siblings().removeClass('_active');
      filterSet(type);
    });

    function filterSet(type) {
      var $item = $('.course_item');

      if (type === 'all') {
        $item.removeClass('_hide');
      } else {
        $item.addClass('_hide').filter('.' + type).removeClass('_hide');
      }
    }
  }());
  // #endregion [mcourse]

  // #endregion [!]----------[ MENU ]----------[!]

  // #region    [!]----------[ SHOW ]----------[!]

  // #region [smain]
  var $smain = $('.smain');
  var $hmainItm = $('.js_to_show');
  var $hmainItmFromBody = $('.js_from_body');
  var $hmainItmFromSide = $('.js_from_side');

  function smainMbl() {
    smainDestroy();

    $smain.append($hmainItm);

    smainInit(true);
  }
  function smainDsk() {
    smainDestroy();

    $hmainItmFromBody.prependTo('.hmain_body');
    $hmainItmFromSide.prependTo('.hmain_side');

    smainInit();
  }
  function smainInit(flag) {
    if ($smain.children().length > 1) {
      if (flag || !$smain.hasClass('_mbl')) {
        $smain.flickity({
          autoPlay: 5000,
          wrapAround: true,
          setGallerySize: false,
          prevNextButtons: false,
          pauseAutoPlayOnHover: false,
        });
      }
    }
  }
  function smainDestroy() {
    if ($('.smain').hasClass('flickity-enabled')) {
      $smain.flickity('destroy');
    }
  }

  smainInit();
  // #endregion [smain]

  // #region [steam]
  $('.steam_show').flickity({
    pageDots: false,
    groupCells: true,
    cellAlign: 'left',
    imagesLoaded: true,
    percentPosition: false,
  });
  // #endregion [steam]

  // #region [syears]
  var $syears = $('.syears');

  $syears.flickity({
    pageDots: false,
    adaptiveHeight: true,
  }).on('change.flickity', function (event, index) {
    var $item = $('.story_item').eq(index);
    $item.addClass('_active').siblings().removeClass('_active');
  });

  $('.syears_item').on('click', function () {
    var index = $(this).index();
    $syears.flickity('select', index);
  });
  // #endregion [syears]

  // #region [sphoto]
  var $sphotoA = $('.sphoto_show');
  var $sphotoB = $('.sphoto_data');
  var $sphotoItmA = $('.sphoto_show_item');
  var $sphotoItmB = $('.sphoto_data_item');
  var sphotoItmNum = $sphotoItmA.length;

  if (sphotoItmNum > 1) {
    if (sphotoItmNum <= 3) {
      $sphotoA.append($sphotoItmA.clone());
      $sphotoB.append($sphotoItmB.clone());
    }

    $sphotoA.flickity({
      bgLazyLoad: 1,
      pageDots: false,
      wrapAround: true,
      imagesLoaded: true,
      prevNextButtons: false,
    });
    $sphotoB.flickity({
      pageDots: false,
      draggable: false,
      wrapAround: true,
      adaptiveHeight: true,
      prevNextButtons: false,
      asNavFor: '.sphoto_show',
    });

    $('.sphoto_prev').on('click', function () {
      $sphotoA.flickity('previous', true);
    });
    $('.sphoto_next').on('click', function () {
      $sphotoA.flickity('next', true);
    });
  }
  // #endregion [sphoto]

  // #endregion [!]----------[ SHOW ]----------[!]

  // #region    [!]----------[ TABS ]----------[!]

  // #region [tteam]
  $('.js_tteam').on('change', function () {
    var index = $(this).val();
    // $beep = $('.team_beep'),
    var $item = $('.team_tab').eq(index);

    $item.addClass('_active').siblings().removeClass('_active');
    // $item.append($beep);
  });
  // #endregion [tteam]

  // #endregion [!]----------[ TABS ]----------[!]

  // #region    [!]----------[ POPUP ]----------[!]
  $('.popup').remodal();

  // #region [pvideo]
  function pvideoShow() {
    fnHideScroll();
    $('.pvideo').addClass('_show');
  }
  function pvideoHide() {
    $('.pvideo').removeClass('_show');
    setTimeout(function () {
      fnShowScroll();
    }, 400);
  }
  $('.js_open_video').on('click', function () {
    var $this = $(this);
    var src = $this.data('src');
    ytPlayerVideo(src);
    pvideoShow();
    return false;
  });
  $('.js_pvideo_close').on('click', function () {
    pvideoHide();
    if ($('#youtube').length) {
      ytPlayerStop();
    }
  });
  // #endregion [pvideo]

  // #endregion [!]----------[ POPUP ]----------[!]

  // #region    [!]----------[ FORM CONF ]----------[!]

  // #region [fn]
  var validTimeout;

  function fnFormReset(form) {
    form[0].reset();
    form.find('select').selectOrDie('update');
  }

  function fnFormSubmit(form, config) {
    var $form = $(form);
    var cfg = config || !1;
    var fnFail = cfg.fail || !1;
    var fnDone = cfg.done || !1;
    var data = $form.serialize();
    var url = $form.attr('action');
    var method = $form.attr('method') || 'post';

    if (cfg.preloader) {
      preloaderShow();
    }

    $.ajax({
      url: url,
      data: data,
      type: method,
      dataType: 'json',
      error: function (response) {
        if (typeof fnFail === 'function') {
          fnFail(response);
        }
      },
      success: function (response) {
        if (response.success === 'Y') {
          fnFormReset($form);
        }

        if (response.redirect) {
          document.location = response.redirect;
        }
        if (response.alert_mess) {
          alert(response.alert_mess[0].message);
        }
        if (typeof fnDone === 'function') {
          fnDone(response);
        }
      },
      complete: function () {
        if (cfg.preloader) {
          preloaderHide();
        }
      },
    });
  }

  function fnFormValidate(form, config) {
    var $form = $(form);
    var $submit = $form.find(':submit');
    var $error = $form.find('.form_error');

    $form.validate({
      submitHandler: function () {
        fnFormSubmit(form, config);
      },
      errorPlacement: function () {
        return false;
      },
    });

    $form.on('keyup change', function () {
      clearTimeout(validTimeout);

      validTimeout = setTimeout(function () {
        if ($form.valid()) {
          $error.hide();

          if (!$submit.hasClass('_stop')) {
            $submit.removeClass('_disabled');
          }
        } else {
          $submit.addClass('_disabled');
          $error.css('display', 'block');
        }
      }, 200);
    });
  }

  function fnFormMessage($box, title, text) {
    var html = '<button class="umsg_close js_umsg_close" type="button"></button>';

    if (title || text) {
      if (title) {
        html += '<span class="umsg_name">' + title + '</span>';
      }
      if (text) {
        html += '<p>' + text + '</p>';
      }

      $box.append(
        '<div class="umsg">'
          + '<div class="umsg_box">' + html + '</div>'
          + '<div class="umsg_overlay js_umsg_close"></div>'
        + '</div>'
      );
    }
  }

  $(document).on('click', '.js_umsg_close', function () {
    $(this).closest('.umsg').remove();
  });
  // #endregion [fn]

  // #region [config]
  $.validator.setDefaults({
    errorClass: '_error',
    validClass: '_valid',
    highlight: function (element, errorClass, validClass) {
      var type = element.type;
      var $this = $(element);
      var $item = $this;

      if (type === 'select-one') {
        $item = $this.parent('.sod_select');
      } else
      if (type === 'radio' || type === 'checkbox') {
        $item = this.findByName(element.name).parent('.radio, .checkbox');
      }

      $item.add($this).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      var type = element.type;
      var $this = $(element);
      var $item = $this;

      if (type === 'select-one') {
        $item = $this.parent('.sod_select');
      } else
      if (type === 'radio' || type === 'checkbox') {
        $item = this.findByName(element.name).parent('.radio, .checkbox');
      }

      $item.add($this).addClass(validClass).removeClass(errorClass);
    },
  });

  $.validator.addMethod('r_ltrs', function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z\s]+$/i.test(value);
  }, 'Only letters are allowed');
  $.validator.addMethod('r_alnm', function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z0-9\s]+$/i.test(value);
  }, 'Only letters and digits are allowed');
  $.validator.addMethod('r_text', function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z0-9\s\.,\-\(\)]+$/i.test(value);
  }, 'Only letters, nembers and punctuation are allowed');

  $.validator.addMethod('r_date', function (value, element) {
    return this.optional(element) || /(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d/i.test(value);
  }, 'Incorrect date');
  $.validator.addMethod('r_phone', function (value, element) {
    return this.optional(element) || /^(\+?\d{1,3})\s?(\(?\d{2,3}\)?)\s?(\d{3}(\s|-)?\d{2}(\s|-)?\d{2})$/i.test(value);
  }, 'Incorrect phone');

  $.validator.addClassRules('v_url', { url: true });
  $.validator.addClassRules('v_age', { r_age: true });
  $.validator.addClassRules('v_ltrs', { r_ltrs: true });
  $.validator.addClassRules('v_alnm', { r_alnm: true });
  $.validator.addClassRules('v_text', { r_text: true });
  $.validator.addClassRules('v_dgts', { digits: true });
  $.validator.addClassRules('v_phone', { r_phone: true });
  // #endregion [config]

  // #endregion [!]----------[ FORM CONF ]----------[!]

  // #region    [!]----------[ FORM ITEM ]----------[!]

  // #region [phone]
  $('.js_phone').mask('+375 (99) 999-99-99');
  // #endregion [phone]

  // #region [select]
  $('option:empty').html(' ');
  $('select').selectOrDie();

  $('form').on('reset', function () {
    $('select').selectOrDie('update');
  });
  $(document).on('change', 'select[required]', function () {
    $(this).valid();
  });

  // #endregion [select]

  // #region [eCoupon]
  var eCouponTimeout;

  $('.eCoupon_btn').on('click', function () {
    var $this = $(this);
    var $inpt = $this.prev();
    var $form = $this.closest('form');
    var $cost = $form.find('.js_cost');
    var url = $form.attr('action');
    var value = $inpt.val();

    if (value !== '') {
      $this
        .addClass('_load')
        .removeClass('_done _fail');

      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
          coupon: value,
          id: $('#idprgr').val(), // !!
          checkcoupon: true, // !!
        },
        success: function (o) {
          if (o.success === 'Y') {
            $cost.html(o.cost);
            $('.pr_val').val(o.price);// !!

            $this.addClass('_done');
          } else {
            $this.addClass('_fail');

            clearTimeout(eCouponTimeout);
            eCouponTimeout = setTimeout(function () {
              $this.removeClass('_fail');
            }, 2000);
          }
        },
        complete: function () {
          $this.removeClass('_load');
        },
      });
    } else {
      $inpt.addClass('_error');
    }

    return false;
  });

  $('.eCoupon_input').on('change', function () {
    var $this = $(this);

    if ($this.val() !== '') {
      $this.removeClass('_error');
    }
  });
  // #endregion [eCoupon]

  // #region [radio & checkbox]
  $(':radio, :checkbox')
    .iCheck({
      radioClass: 'radio',
      hoverClass: '_hover',
      checkedClass: '_checked',
      checkboxClass: 'checkbox',
      disabledClass: '_disabled',
    })
    .filter('[required]')
    .on('ifChanged', function () {
      $(this).valid();
    });
  // #endregion [radio & checkbox]

  // #endregion [!]----------[ FORM ITEM ]----------[!]

  // #region    [!]----------[ FORM LIST ]----------[!]

  // #region [ffaq]
  fnFormValidate('#ffaq', {
    preloader: true,
    done: function (data) {
      fnFormMessage(
        $('#ffaq .form_box'),
        data.msg.title,
        data.msg.text
      );
    },
  });
  // #endregion [ffaq]

  // #region [ffind]
  var ffindTimeout;
  $('.ffind_submit').on('click', function () {
    var $this = $(this);

    if (!$this.hasClass('_active')) {
      $this.addClass('_active');
      $('.ffind').addClass('_show');

      clearTimeout(ffindTimeout);
      ffindTimeout = setTimeout(function () {
        $('.ffind_input').focus();
      }, 200);

      return false;
    }
  });
  $(document).on('click', function (e) {
    var $ffind = $('.ffind');
    if (!$ffind.is(e.target) && $ffind.has(e.target).length === 0) {
      $('.ffind').removeClass('_show');
      $('.ffind_submit').removeClass('_active');
    }
  });
  // #endregion [ffind]

  // #region [forder]
  fnFormValidate('#forder', {
    preloader: true,
    done: function (data) {
      if (data) {
        fnFormMessage(
          $('#forder .form_box'),
          data.msg.text
        );
      }
    },
    fail: function (data) {
      if (data) {
        fnFormMessage(
          $('#forder .form_box'),
          data.msg.text
        );
      }
    },
  });

  $('.eCoupon_input').on('keyup focusout', function () {
    var $this = $(this);
    var $btn = $this.next();

    var value = $this.val();

    if (value === '') {
      $btn.prop('disabled', true);
    } else {
      $btn.prop('disabled', false);
    }
  });

  $('.wPayment_menu_radio').on('ifChecked', function () {
    var $this = $(this).closest('.wPayment_menu_lnk');
    var $form = $this.closest('form');
    var $tab = $('.wPayment_tabs_itm');
    var $submit = $('.wPayment_submit');

    var type = $this.data('type');
    var index = $this.index();

    $tab
      .eq(index)
      .add($this)
      .addClass('_active')
      .siblings()
      .removeClass('_active');

    $submit
      .removeClass('_stop wPayment_submit-request wPayment_submit-payment');

    if ($form.valid()) {
      $submit.removeClass('_disabled');
    }

    if (type === 1) {
      $submit.addClass('wPayment_submit-request');
    } else {
      $submit.addClass('wPayment_submit-payment');
    }

    if (!$('.eCoupon_btn').hasClass('_done')) { // !!
      $('[name=code]').val('');
    }
  });

  /*
  $('.forder_submit').on('click', function () {
    var type = $(this).attr('data-type');
    $('.js_forder_type').val(type);
  });
  */
  // #endregion [forder]

  // #endregion [!]----------[ FORM LIST ]----------[!]

  // #region    [!]----------[ RESPONSE ]----------[!]
  var $mlang = $('.mlang');
  var $mabout = $('.mabout');
  var $mnets = $('.lheader_nets');
  var $aboutName = $('.about_name');
  var $mmainWrap = $('.mmain_wrap');
  var $mmainFoot = $('.mmain_foot');
  var $lheaderBar = $('.lheader_bar');
  var $maboutBox = $('.js_mabout_box');

  enquire.register(mobile, {
    match: function () {
      smainMbl();
      runIfFunc(mpageShowOn);

      $mlang.prependTo($mmainWrap);
      $mnets.prependTo($mmainFoot);
      $aboutName.after($mabout);

      $('.js_show_sub').on('click', function () {
        $(this).next().add('.mmain_back').addClass('_show');
        return false;
      });

      $(window).off('resize.msub');
    },
  });
  enquire.register(tablet, {
    match: function () {
      smainDsk();
      runIfFunc(mpageShowOff);

      mmainReset();
      $('.js_show_sub').off('click');

      $lheaderBar.append($mlang, $mnets);
      $mabout.appendTo($maboutBox);

      $(window)
        .on('load', function () {
          msubMinHeight();
        })
        .on('resize.msub', function () {
          clearTimeout(msubTimeout);
          msubTimeout = setTimeout(function () {
            msubMinHeight();
          }, 200);
        });
    },
  });
  // #endregion [!]----------[ RESPONSE ]----------[!]

});
